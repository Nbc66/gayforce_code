//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "hud_numericdisplay.h"
#include "iclientmode.h"

#include <Color.h>
#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui/IVGui.h>

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudNumericDisplay::CHudNumericDisplay(vgui::Panel *parent, const char *name) : BaseClass(parent, name)
{
	vgui::Panel *pParent = g_pClientMode->GetViewport();
	SetParent( pParent );

	m_iValue = 0;
	m_LabelText[0] = 0;
	m_iSecondaryValue = 0;
	m_bDisplayValue = true;
	m_bDisplaySecondaryValue = false;
	m_bIndent = false;
	m_bIsTime = false;
}

//-----------------------------------------------------------------------------
// Purpose: Resets values on restore/new map
//-----------------------------------------------------------------------------
void CHudNumericDisplay::Reset()
{
	m_flBlur = 0.0f;
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudNumericDisplay::SetDisplayValue(int value)
{
	m_iValue = value;
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudNumericDisplay::SetSecondaryValue(int value)
{
	m_iSecondaryValue = value;
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudNumericDisplay::SetShouldDisplayValue(bool state)
{
	m_bDisplayValue = state;
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudNumericDisplay::SetShouldDisplaySecondaryValue(bool state)
{
	m_bDisplaySecondaryValue = state;
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudNumericDisplay::SetLabelText(const wchar_t *text)
{
	wcsncpy(m_LabelText, text, sizeof(m_LabelText) / sizeof(wchar_t));
	m_LabelText[(sizeof(m_LabelText) / sizeof(wchar_t)) - 1] = 0;
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudNumericDisplay::SetIndent(bool state)
{
	m_bIndent = state;
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudNumericDisplay::SetIsTime(bool state)
{
	m_bIsTime = state;
}

//-----------------------------------------------------------------------------
// Purpose: paints a number at the specified position
//-----------------------------------------------------------------------------
void CHudNumericDisplay::PaintNumbers(HFont font, int xpos, int ypos, int value)
{
	surface()->DrawSetTextFont(font);
	wchar_t unicode[6];
	if ( !m_bIsTime )
	{
		V_snwprintf(unicode, ARRAYSIZE(unicode), L"%d", value);
	}
	else
	{
		int iMinutes = value / 60;
		int iSeconds = value - iMinutes * 60;
#ifdef PORTAL
		// portal uses a normal font for numbers so we need the seperate to be a renderable ':' char
		if ( iSeconds < 10 )
			V_snwprintf( unicode, ARRAYSIZE(unicode), L"%d:0%d", iMinutes, iSeconds );
		else
			V_snwprintf( unicode, ARRAYSIZE(unicode), L"%d:%d", iMinutes, iSeconds );		
#else
		if ( iSeconds < 10 )
			V_snwprintf( unicode, ARRAYSIZE(unicode), L"%d`0%d", iMinutes, iSeconds );
		else
			V_snwprintf( unicode, ARRAYSIZE(unicode), L"%d`%d", iMinutes, iSeconds );
#endif
	}

	// adjust the position to take into account 3 characters
	int charWidth = surface()->GetCharacterWidth(font, '0');
	if (value < 100 && m_bIndent)
	{
		xpos += charWidth;
	}
	if (value < 10 && m_bIndent)
	{
		xpos += charWidth;
	}

	surface()->DrawSetTextPos(xpos, ypos);
	surface()->DrawUnicodeString( unicode );
}

//-----------------------------------------------------------------------------
// Purpose: draws the text
//-----------------------------------------------------------------------------
void CHudNumericDisplay::PaintLabel( void )
{
	float wave2 = gpGlobals->curtime * 2;
	int expandfull = 8;
	int expand = 1 + floor(abs(sinf(wave2) * expandfull));

	for (int i = 0; i < expand; i++){
		surface()->DrawSetTextFont(m_hTextFont);
		surface()->DrawSetTextColor(Color(0, 0, 0, 255));
		surface()->DrawSetTextPos((text_xpos + 1) + i, (text_ypos) + -i);
		surface()->DrawUnicodeString(m_LabelText);
		surface()->DrawSetTextPos((text_xpos - 1) + i, (text_ypos) + -i);
		surface()->DrawUnicodeString(m_LabelText);
		surface()->DrawSetTextPos((text_xpos) + i, (text_ypos + 1) + -i);
		surface()->DrawUnicodeString(m_LabelText);
		surface()->DrawSetTextPos((text_xpos) + i, (text_ypos - 1) + -i);
		surface()->DrawUnicodeString(m_LabelText);
	}

	for (int i = 0; i < expand; i++){
		surface()->DrawSetTextFont(m_hTextFont);
		surface()->DrawSetTextColor(Color(254 - (i * 31.88), i * 31.88, 255, 255));
		surface()->DrawSetTextPos(text_xpos + i, text_ypos + -i);
		surface()->DrawUnicodeString( m_LabelText );
	}

}

//-----------------------------------------------------------------------------
// Purpose: renders the vgui panel
//-----------------------------------------------------------------------------
void CHudNumericDisplay::Paint()
{

	float wave2 = gpGlobals->curtime * 2;
	int expandfull = 8;
	int expand = 1 + floor(abs(sinf(wave2) * expandfull));
	//surface()->DrawSetTextColor(Color(floor(RandomFloat(0, 2)) * 128.0f, floor(RandomFloat(0, 2)) * 128.0f, floor(RandomFloat(0, 2)) * 128.0f, 255));
	//surface()->DrawSetTextColor(Color((128 * sin(wave2)) + 127, 128 * sin(wave2 + (3.14 / 2)) + 127, 128 * sin(wave2 + 3.14) + 127, 255));
	if (m_bDisplayValue)
	{
		// draw our numbers
		//surface()->DrawSetTextColor(GetFgColor());
		//surface()->DrawSetTextColor(Color(128, 0, 128, 255));
		//PaintNumbers(m_hNumberFont, digit_xpos, digit_ypos, m_iValue);

		for (int i = 0; i < expand; i++){
			surface()->DrawSetTextColor(Color(0,0,0,255));
			PaintNumbers(m_hNumberFont, (digit_xpos + 1) + i, (digit_ypos) + -i, m_iValue);
			PaintNumbers(m_hNumberFont, (digit_xpos - 1) + i, (digit_ypos)+-i, m_iValue);
			PaintNumbers(m_hNumberFont, (digit_xpos) + i, (digit_ypos + 1)+-i, m_iValue);
			PaintNumbers(m_hNumberFont, (digit_xpos) + i, (digit_ypos - 1)+-i, m_iValue);
		}

		//PaintNumbers(m_hNumberFont, digit_xpos + (-8 * abs(sinf(wave2))), digit_ypos + (-8 * abs(sinf(wave2))), m_iValue);
		for (int i = 0; i < expand; i++){
			surface()->DrawSetTextColor(Color(254 - (i * 31.88), i * 31.88, 255, 255));
			PaintNumbers(m_hNumberFont, digit_xpos + i, digit_ypos + -i, m_iValue);
		}

		// draw the overbright blur
		for (float fl = m_flBlur; fl > 0.0f; fl -= 1.0f)
		{
			if (fl >= 1.0f)
			{
				//PaintNumbers(m_hNumberGlowFont, digit_xpos, digit_ypos, m_iValue);
			}
			else
			{
				// draw a percentage of the last one
				//Color col = GetFgColor();
				//col[3] *= fl;
				//surface()->DrawSetTextColor(Col);
				//PaintNumbers(m_hNumberGlowFont, digit_xpos, digit_ypos, m_iValue);
			}
		}
	}

	// total ammo
	if (m_bDisplaySecondaryValue)
	{
		//surface()->DrawSetTextColor(Col);
		//PaintNumbers(m_hSmallNumberFont, digit2_xpos, digit2_ypos, m_iSecondaryValue);

		for (int i = 0; i < expand; i++){
			surface()->DrawSetTextColor(Color(0, 0, 0, 255));
			PaintNumbers(m_hSmallNumberFont, (digit2_xpos + 1) + i, (digit2_ypos)+-i, m_iSecondaryValue);
			PaintNumbers(m_hSmallNumberFont, (digit2_xpos - 1) + i, (digit2_ypos)+-i, m_iSecondaryValue);
			PaintNumbers(m_hSmallNumberFont, (digit2_xpos)+i, (digit2_ypos + 1) + -i, m_iSecondaryValue);
			PaintNumbers(m_hSmallNumberFont, (digit2_xpos)+i, (digit2_ypos - 1) + -i, m_iSecondaryValue);
		}

		//PaintNumbers(m_hNumberFont, digit_xpos + (-8 * abs(sinf(wave2))), digit_ypos + (-8 * abs(sinf(wave2))), m_iValue);
		for (int i = 0; i < expand; i++){
			surface()->DrawSetTextColor(Color(254 - (i * 31.88), i * 31.88, 255, 255));
			PaintNumbers(m_hSmallNumberFont, digit2_xpos + i, digit2_ypos + -i, m_iSecondaryValue);
		}

	}

	PaintLabel();
}



