//=============================================================================//
//
// Purpose: Ask: This is a screen we'll use for the RPG
//
//=============================================================================//
#include "cbase.h"
#ifdef CLIENT_DLL
#include "C_VGuiScreen.h"
#include "c_baseplayer.h"
#endif
#include <vgui/IVGUI.h>
#include <vgui_controls/Controls.h>
#include <vgui_controls/Label.h>
#include "clientmode_hlnormal.h"
#include "baseviewmodel_shared.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//-----------------------------------------------------------------------------
//
// In-game vgui panel which shows the RPG's ammo count
//
//-----------------------------------------------------------------------------
class CSMGScreen : public CVGuiScreenPanel
{
    DECLARE_CLASS( CSMGScreen, CVGuiScreenPanel );

public:
    CSMGScreen( vgui::Panel *parent, const char *panelName );

    virtual bool Init( KeyValues* pKeyValues, VGuiScreenInitData_t* pInitData );
    virtual void OnTick();
    virtual void SetSkin(int ammocount);

private:
    vgui::Label *m_pAmmoCount;
    CBaseViewModel *pViewModel;
    CBasePlayer* pPlayer;
};


//definitions for color for vgui
//-Nbc66
#define LOW_AMMO_YELLOW  20
#define LOW_AMMO_RED     10
#define RED              255, 106, 0, 255
#define YELLOW           255, 183, 0, 255
#define DEFAULT_COLOR    255, 255, 255, 255

//-----------------------------------------------------------------------------
// Standard VGUI panel for objects
//-----------------------------------------------------------------------------
DECLARE_VGUI_SCREEN_FACTORY( CSMGScreen, "smg_screen" );


//-----------------------------------------------------------------------------
// Constructor:
//-----------------------------------------------------------------------------
CSMGScreen::CSMGScreen( vgui::Panel *parent, const char *panelName ) : BaseClass( parent, panelName, g_hVGuiCombineScheme )
{
}

#ifdef CLIENT_DLL
//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------
bool CSMGScreen::Init( KeyValues* pKeyValues, VGuiScreenInitData_t* pInitData )
{
    // Load all of the controls in
    if ( !BaseClass::Init(pKeyValues, pInitData) )
        return false;

    // Make sure we get ticked...
    vgui::ivgui()->AddTickSignal( GetVPanel() );

    // Ask: Here we find a pointer to our AmmoCountReadout Label and store it in m_pAmmoCount
    m_pAmmoCount =  dynamic_cast<vgui::Label*>(FindChildByName( "AmmoCountReadout" ));

    return true;
}
#endif
//-----------------------------------------------------------------------------
// Ask: Let's update the label, shall we?
//-----------------------------------------------------------------------------
void CSMGScreen::OnTick()
{

    BaseClass::OnTick();

    // Get our player
    pPlayer = C_BasePlayer::GetLocalPlayer();
    if ( !pPlayer )
        return;

    // Get the players active weapon
    CBaseCombatWeapon *pWeapon = pPlayer->GetActiveWeapon();

    // If pWeapon is NULL or it doesn't use primary ammo, don't update our screen
    if ( !pWeapon || !pWeapon->UsesPrimaryAmmo() )
        return;

    // sets ammo1 to the actauly clip size
    // Good thing this updates every tick or the numbers would be static lol
    // -Nbc66
    int ammo1 = pWeapon->Clip1();

    // If our Label exist
    if ( m_pAmmoCount )
    {
        char buf[32];
        Q_snprintf( buf, sizeof( buf ), "%d", ammo1 );
        // Set the Labels text to the number of missiles we have left.
        m_pAmmoCount->SetText( buf );
    }

    if (ammo1 <= LOW_AMMO_RED)
    {
       m_pAmmoCount->SetFgColor(Color(RED));
       SetSkin(1);
    }
    else if (ammo1 <= LOW_AMMO_YELLOW)
    {
        m_pAmmoCount->SetFgColor(Color(YELLOW));
        SetSkin(2);
    }
    else
    {
        m_pAmmoCount->SetFgColor(Color(DEFAULT_COLOR));
        SetSkin(0);
    }
    
}

//sets viewmodel skin based on ammocount left in the weapon 
//function should be updated every tick so that its allways up to date.
//Don't know how it will impact performance tho calling it every frame
//-Nbc66
void CSMGScreen::SetSkin(int skin)
{
    pPlayer = C_BasePlayer::GetLocalPlayer();
    pViewModel = pPlayer->GetViewModel();


    //I HAVE NO FUCKING CLUE WHY IT WORKS THIS WAY BUT IT DOSE WTF VALVE
    //-Nbc66
    if ( skin == 1 )
    {
        pViewModel->m_nSkin = 2;
        //ConDColorMsg(Color(255, 140, 0, 255), "Current viewmodel skin = %i\n", pViewModel->GetSkin());
    }
    else if( skin == 2 )
    {
        pViewModel->m_nSkin = 1;
        //ConDColorMsg(Color(255, 140, 0, 255), "Current viewmodel skin = %i\n", pViewModel->GetSkin());
    }
    else
    {
        pViewModel->m_nSkin = 0;
    }
    
}
